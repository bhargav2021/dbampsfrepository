

CREATE PROCEDURE [sp_pdw_sm_detach]

    -- Parameters

    @FileName nvarchar(45)  -- shared memory name

    AS

        SET NOCOUNT ON;

        EXEC [sp_sm_detach] @FileName;

